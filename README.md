vidi-timeline creates a video timeline starting from a MIDI file.

The video clips are taken from a "VideoFont" in which each sample clip
corresponds to a note. The samples are arranged following the time and value of
the notes in the MIDI file.

vidi-timeline allows to create more easily videos like these:

* [Lasse Gjertsen - Hyperactive](https://youtu.be/o9698TqtY4A)
* [Vittorio Saggiomo - Mario Bros Column Chromatography](https://youtu.be/mxi3z2vDV_0)


How to create a VideoFont
=========================

Synthetic VideoFont
-------------------

Create a synthetic VideoFont:

    $ ./create_test_videofont.py videofont/


From a video recording
----------------------

A VideoFont can also be created by recording a video, and then splitting the
recording in samples, one sample per note.

For an example of a master VideoFont see
[keyboard videofont master C4 B5](https://youtu.be/7Btp80LPqRs)

A file like the one above can be analyzed with [Audacity][1] to find the start
and the  end time of the individual samples:

* use the `Analyze -> Sound Finder...` to find the samples;
* use the [`Pitch Detect" plugin`][2] to find the pitch of the samples and name
  them accordingly;
* maybe add  an interval named "rest" to represent the absence of sound;
* export the labels track with `File -> Export Labels...`;

[1]: http://www.audacityteam.org/
[2]: http://wiki.audacityteam.org/wiki/Nyquist_Analyze_Plug-ins#Pitch_Detect

An example of such a file produced by audacity can be found in the `contrib/`
directory and it can be used as follows:

    $ youtube-dl -t https://youtu.be/7Btp80LPqRs
    $ ./contrib/ges-split-samples.py \
        "keyboard videofont master C4 B5-7Btp80LPqRs.mp4" \
        "contrib/keyboard videofont master C4 B5-samples.txt" \
        videofont/ > split.sh
    $ mkdir videofont/
    $ sh split.sh && rm split.sh


Examples of use
===============

vidi-timeline.py
----------------

Play the timeline from a MIDI file using the samples from the VideoFont:

    $ ./vidi-timeline.py examples/Beyer\ Op.\ 101\ -\ Exercise\ 008.midi videofont/


Save the timeline to be edited somewhere else (e.g. in PiTiVi):

    $ ./vidi-timeline.py examples/Beyer\ Op.\ 101\ -\ Exercise\ 008.midi videofont/ Beyer_008.xges


Render the timeline to a video file:

    $ ges-launch-1.0 --load Beyer_008.xges --outputuri Beyer_008.webm --format="video/webm:video/x-vp8:audio/x-vorbis"


vidi-player.py
--------------

Play a midi file in real time (if the CPU and the disk can keep up):

    $ ./vidi-player.py examples/Beyer\ Op.\ 101\ -\ Exercise\ 008.midi videofont/


vidi-sampler.py
---------------

Play samples from the note hit on a midi controller:

    $ xset -r && vkeybd && xset r on &
    $ ./vidi-sampler.py 'Virtual Keyboard' videofont/
    $ fg


Similar projects
================

Of course if the idea is cool, chances are that others had it too. :)

Midi-Vidi
---------

A similar concept called midi-vidi has been developed by Marcus Fischer:

* <http://unrecnow.com/dust/325-midi-vidi-effect-demo/>
* <https://vimeo.com/8049917>
* <http://www.synthtopia.com/content/2009/12/09/midi-vidi-plugin-lets-you-control-video-via-midi-in-max-for-live/>
* <http://www.maxforlive.com/library/device.php?id=120>

Midi-Vidi is also a very cool name, it sound like the Italian "mi dividi" which
means "you divide me".

VidiBox
-------

An interactive video mashup app:

* <http://www.vidibox.net/>
* <https://twitter.com/vidibox>
