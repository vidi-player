#!/usr/bin/env python3
#
# ges-split-samples - generate commands to split a video file into samples
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

CLIP_FORMAT = "video/webm:video/x-vp8:audio/x-vorbis"
CLIP_FILE_EXTENSION = "webm"
CLIP_FILENAME_TEMPLATE = "sample_%s.%s"


def parse_audacity_labels(samples_list_filename):
    """Parse labels exported from Audacity.

    NOTE: Audacity uses the current user locale when exporting the labels, but
    this way there is no portable way to parse the output, so we just assume
    here that the C locale was used.

    Basically run "LANG=C audacity" before exporting the data.
    """
    samples = []

    with open(samples_list_filename, "r") as samples_list_file:
        for row in samples_list_file:
            if row.startswith('#'):
                continue

            start_time, end_time, sample_name = row.split()

            start_time = float(start_time)
            end_time = float(end_time)

            samples.append((sample_name, start_time, end_time))

    return samples


def usage():
    print("usage: %s <video_file> <samples_list_file> <destination_dir>"
          % os.path.basename(sys.argv[0]))
    print("sample_list_file is in the format used by Audacity when exporting Label Tracks")


def main():
    if len(sys.argv) > 1 and sys.argv[1] in ["-h", "--help"]:
        usage()
        return 0

    if len(sys.argv) < 4:
        usage()
        return 1

    master_file = sys.argv[1]
    samples_list_filename = sys.argv[2]
    destination_dir = sys.argv[3]

    samples = parse_audacity_labels(samples_list_filename)
    for sample_name, start_time, end_time in samples:
        duration = round(end_time - start_time, 2)
        clip_filename = CLIP_FILENAME_TEMPLATE % (sample_name,
                                                  CLIP_FILE_EXTENSION)
        clip_path = os.path.join(destination_dir, clip_filename)
        print("ges-launch-1.0 +clip \"%s\" inpoint=%s duration=%s -o \"%s\" --format=\"%s\"" %
              (master_file, start_time, duration, clip_path, CLIP_FORMAT))

        if sample_name == "rest":
            rest_sample_filename = CLIP_FILENAME_TEMPLATE % (sample_name, "png")
            rest_sample_path = os.path.join(destination_dir,
                                            rest_sample_filename)
            print("gst-launch-1.0 filesrc location=\"%s\" ! decodebin ! videoconvert ! pngenc snapshot=1 ! filesink location=\"%s\"" %
                  (clip_path, rest_sample_path))


if __name__ == "__main__":
    sys.exit(main())
