#!/usr/bin/env python3

import sys
import os
import vidi

#FONT_DESC = "Andale Mono, 72"
FONT_DESC = "Mono, 72"

SAMPLE_LENGTH_SECONDS = 4

VIDEO_FRAMERATE = 25
VIDEO_BUFFERS = SAMPLE_LENGTH_SECONDS * VIDEO_FRAMERATE

AUDIO_SAMPLERATE = 48000
AUDIO_SAMPLESPERBUFFER = 1024
AUDIO_BUFFERS = round(SAMPLE_LENGTH_SECONDS * 48000 / 1024)

LIVE_PIPELINE_TEMPLATE = """
  videotestsrc num-buffers=%d pattern=black ! video/x-raw,framerate=%d/1 ! \
          textoverlay valignment=center halignment=center font-desc="%s" text="{0}" ! \
          autovideosink \
  audiotestsrc num-buffers=%d samplesperbuffer=%d freq={1:f} ! audio/x-raw,rate=%d ! \
          autoaudiosink
""" % (VIDEO_BUFFERS, VIDEO_FRAMERATE, FONT_DESC, AUDIO_BUFFERS,
       AUDIO_SAMPLESPERBUFFER, AUDIO_SAMPLERATE)

FILE_PIPELINE_TEMPLATE = """
  webmmux name=mux ! filesink location="{2}/sample_{0}.webm"
  videotestsrc num-buffers=%d pattern=black ! video/x-raw,framerate=%d/1 ! \
          textoverlay valignment=center halignment=center font-desc="%s" text="{0}" ! \
          queue ! vp9enc ! mux.
  audiotestsrc num-buffers=%d samplesperbuffer=%d freq={1:f} ! audio/x-raw,rate=%d ! \
          queue ! audioconvert ! vorbisenc quality=0.5 ! queue ! mux.
""" % (VIDEO_BUFFERS, VIDEO_FRAMERATE, FONT_DESC, AUDIO_BUFFERS,
       AUDIO_SAMPLESPERBUFFER, AUDIO_SAMPLERATE)

PNG_REST_SAMPLE_TEMPLATE = """
  videotestsrc num-buffers=1 pattern=black ! \
          textoverlay valignment=center halignment=center font-desc="%s" text="rest" ! \
          pngenc ! filesink location="{0}/sample_rest.png"
""" % FONT_DESC


def create_test_videofont(pipeline_template, notes_range, destination_dir=None):
    print("Silence")
    pipeline_string = pipeline_template.format("rest", 0, destination_dir)
    player = vidi.Player.from_pipeline_string(pipeline_string)
    player.play()

    for i, note_number in enumerate(notes_range):
        note = vidi.SpnNote(note_number)

        print("%2d %s" % (i, note))

        pipeline_string = pipeline_template.format(note.name, note.frequency, destination_dir)

        player = vidi.Player.from_pipeline_string(pipeline_string)
        error = player.play()
        if error:
            break


def usage():
    print("usage: %s [<videofont_destination_dir>]" %
          os.path.basename(sys.argv[0]))


def main():
    if len(sys.argv) > 1 and sys.argv[1] in ["-h", "--help"]:
        usage()
        return 0

    notes_range = vidi.PIANO_88_KEYS_RANGE

    if len(sys.argv) > 1:
        destination_dir = os.path.realpath(sys.argv[1])
        if os.path.exists(destination_dir):
            sys.stderr.write("The destination already exists '%s'!\n"
                             % destination_dir)
            return 1

        os.mkdir(destination_dir)
        create_test_videofont(FILE_PIPELINE_TEMPLATE, notes_range,
                              destination_dir)

        pipeline_string = PNG_REST_SAMPLE_TEMPLATE.format(destination_dir)
        player = vidi.Player.from_pipeline_string(pipeline_string)
        player.play()
    else:
        create_test_videofont(LIVE_PIPELINE_TEMPLATE, notes_range)


if __name__ == "__main__":
    sys.exit(main())
