#!/usr/bin/env python3
#
# ges-simple-player - simple GStreamer Editing Service example
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import gi

# XXX it's an ugly hack, I know
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import vidi

gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

gi.require_version('GES', '1.0')
from gi.repository import GES
GES.init()

def play_clip(video_path):

    timeline = GES.Timeline.new_audio_video()
    layer = timeline.append_layer()

    video_uri = Gst.filename_to_uri(video_path)
    asset = GES.UriClipAsset.request_sync(video_uri)
    clip = layer.add_asset(asset, 0, 0, asset.get_duration(), GES.TrackType.UNKNOWN)

    timeline.commit()

    pipeline = GES.Pipeline()
    pipeline.set_timeline(timeline)

    vidi.Player(pipeline).play()


def main():
    if len(sys.argv) < 2:
        return 1

    video_path = os.path.realpath(sys.argv[1])
    play_clip(video_path)


if __name__ == "__main__":
    sys,exit(main())
