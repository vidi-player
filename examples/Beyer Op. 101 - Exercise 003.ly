\version "2.19.51"

\score {
  \relative c'' {
    \time 4/4
    \tempo 4 = 120
    c1 d e f
    g f e d
    c
  }
  \layout { }
  \midi { }
}
