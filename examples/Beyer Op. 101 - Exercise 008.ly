\version "2.19.51"

\score {
  \relative c'' {
    \time 4/4
    \tempo 4 = 120
    c4 ( -1 e4 -3 c4 -1 e4 -3
    g4 -5 c,4 ) c4 c4 (
    d4 ) d4 d4 d4 (
    e4 ) e4 e4 e4 (
    c4 e4 c4 e4
    g4 c,4 ) c4 c4 (
    d4 ) d4 ( e4 d4
    c4 e4 c2 ) \bar "||"
    \break \repeat volta 2 {

      g'4 ( -5 d4 ) -2 d4 d4 (
      | \barNumberCheck #10
      e4 -3 c4 ) -1 c4 c4 (
      g'4 d4 ) d4 d4 (
      e4 c4 e4 d4
      c4 ) ( e4 c4 e4
      g4 c,4 ) c4 c4 (
      d4 ) d4 ( e4 d4
      c4 e4 c2 )
    }
  }
 \layout { }
 \midi { }
}
