#!/usr/bin/env python3
#
# vidi-sampler - play video samples interactively from a midi device
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import sys
import vidi


def usage():
    print("usage: %s <midi_input_device> <videofont_directory>"
          % os.path.basename(sys.argv[0]))


def main():
    if len(sys.argv) > 1 and sys.argv[1] in ["-h", "--help"]:
        usage()
        return 0

    if len(sys.argv) < 3:
        usage()
        return 1

    if not os.path.isdir(sys.argv[2]):
        sys.stderr.write("The second argument must be the path of the videofont directory\n")
        usage()
        return 1

    video_font_path = os.path.realpath(sys.argv[2])
    sampler = vidi.DeviceSampler(video_font_path, sys.argv[1])
    sampler.play()


if __name__ == '__main__':
    sys.exit(main())
