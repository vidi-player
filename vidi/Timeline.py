#!/usr/bin/env python3
#
# Timeline - very simple GES timeline wrapper
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

gi.require_version('GES', '1.0')
from gi.repository import GES
GES.init()

from .Player import Player

TITLE_BACKGROUND = 0xFF000000
TITLE_COLOR = 0xFFFFFFFF
TITLE_OUTLINE_COLOR = 0x00000000
TITLE_FONT_DESC = "Georgia, 24"

class Timeline(object):
    def __init__(self, canvas_size=None):
        self.project = GES.Project(extractable_type=GES.Timeline)
        self.timeline = GES.Asset.extract(self.project)

        audio_track = GES.AudioTrack.new()
        video_track = GES.VideoTrack.new()

        if canvas_size:
            width, height = canvas_size
            caps = Gst.Caps.new_empty_simple("video/x-raw")
            caps.set_value("width", width)
            caps.set_value("height", height)
            video_track.set_restriction_caps(caps)

        self.timeline.add_track(audio_track)
        self.timeline.add_track(video_track)

        self.layer = self.timeline.append_layer()

        ges_pipeline = GES.Pipeline()
        ges_pipeline.set_timeline(self.timeline)
        self.player = Player(ges_pipeline)

    def add_title_clip(self, text, start_time, duration):
        title_clip = GES.TitleClip()
        title_clip.set_start(start_time * Gst.SECOND)
        title_clip.set_duration(duration * Gst.SECOND)
        self.layer.add_clip(title_clip)

        # Now that the clip is inserted in the timeline, it has a source which
        # can be used to set its properties. (comment taken from Pitivi)
        title_source = title_clip.find_track_element(None, GES.TitleSource)
        title_source.set_child_property("text", text)
        title_source.set_child_property("background", TITLE_BACKGROUND)
        title_source.set_child_property("color", TITLE_COLOR)
        title_source.set_child_property("outline-color", TITLE_OUTLINE_COLOR)
        title_source.set_child_property("font-desc", TITLE_FONT_DESC)
        title_source.set_child_property("halignment", GES.TextVAlign.ABSOLUTE)
        title_source.set_child_property("valignment", GES.TextHAlign.ABSOLUTE)

    def add_clip(self, clip_path, start_time, duration):
        clip_uri = Gst.filename_to_uri(clip_path)
        asset = GES.UriClipAsset.request_sync(clip_uri)
        self.layer.add_asset(asset, start_time * Gst.SECOND, 0,
                             duration * Gst.SECOND, GES.TrackType.UNKNOWN)

    def add_layer_clip(self, clip_path, start_time, duration):
        """Add a clip on its own layer"""
        clip_uri = Gst.filename_to_uri(clip_path)
        asset = GES.UriClipAsset.request_sync(clip_uri)
        new_layer = self.timeline.append_layer()
        new_layer.add_asset(asset, start_time * Gst.SECOND, 0,
                            duration * Gst.SECOND, GES.TrackType.UNKNOWN)

    def play(self):
        self.timeline.commit()
        self.player.play()

    def stop(self):
        self.player.stop()

    def save(self, path):
        uri = Gst.filename_to_uri(path)
        self.project.save(self.timeline, uri, None, False)
