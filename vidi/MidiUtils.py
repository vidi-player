#!/usr/bin/env python3
#
# MidiUtils - misc utilities to analyze midi files opened with python-mido
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def is_note(msg):
    return msg.type == 'note_on' or msg.type == 'note_off'


def is_note_on(msg):
    return msg.type == 'note_on' and msg.velocity > 0


def is_note_off(msg):
    return ((msg.type == 'note_on' and msg.velocity == 0) or
            (msg.type == 'note_off'))


def check_overlapping_notes(midi_file):
    previous_note_on = False
    for msg in midi_file:
        if is_note_on(msg) and msg.channel == 0:
            if previous_note_on:
                return True

            previous_note_on = True
        elif is_note_off(msg) and msg.channel == 0:
            previous_note_on = False

    return False
