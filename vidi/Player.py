#!/usr/bin/env python3
#
# Player - a very simple GStreamer player
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

from gi.repository import GObject
GObject.threads_init()


class Player(object):
    def __init__(self, pipeline):
        self.pipeline = pipeline

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message::eos", self.on_eos)
        bus.connect('message::error', self.on_error)

        self.mainloop = GObject.MainLoop()

    @staticmethod
    def from_pipeline_string(pipeline_string):
        pipeline = Gst.parse_launch(pipeline_string)
        return Player(pipeline)

    def on_eos(self, bus, message):
        self.stop()

    def on_error(self, bus, msg):
        (err, debug) = msg.parse_error()
        print("Error: %s" % err)
        self.stop()

    def play(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.mainloop.run()

    def stop(self):
        self.mainloop.quit()
        self.pipeline.set_state(Gst.State.NULL)
